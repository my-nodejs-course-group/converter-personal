import { Moment } from 'moment';
export interface UsersStorage {
  [userId: string]: UserData;
}

export enum FileStatus {
  wait = 'wait',
  pending = 'pending',
  ready = 'ready',
  error = 'error'
}

export interface FileData {
  filePathIn: string;
  filePathOut: string;
  originalName: string;
  status: FileStatus;
}

export interface FilesData {
  [fileId: string]: FileData;
}
export interface UserData {
  files: FilesData;
  accessData: AccessData;
  timeTracker?: Moment;
}

export type FilesStatuses = Array<{
    fileId: string
    status: FileStatus;
    fileName: string;
}>

export interface AccessData {
  id: string,
  name: string | null;
  filesCounter: number;
  extraConvertsCounter: number;
  accessToken: string | null;
  refreshToken: string | null;
  isPaid: boolean | null;
  isAuth: boolean;
}
