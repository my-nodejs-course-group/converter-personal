import express from 'express';
import session from 'express-session';
import morgan from 'morgan';
import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';
import cookieParser from 'cookie-parser';
import { routes } from './routes';
import { routesNS } from './routes/noSecureRoutes';
import { adjustedPassport, passportInit } from './authStrategy/pasport';
import { isUserExist, createUserInStorage, getUserAccessData } from '@src/storage';
import { AccessData } from '@src/storage/types';

const localEnv = dotenv.config();
dotenvExpand.expand(localEnv);
const app = express();
app.use(express.urlencoded());
app.use(express.json());
app.use(morgan('dev'));

app.use(routesNS);

passportInit();
app.use(cookieParser());
app.use(session({
    secret: <string>process.env.SESSION_SECRET_KEY
}));

app.use(adjustedPassport.initialize());
app.use(adjustedPassport.session());
app.use((req, res, next) => {
  let userId = null;
  if (req.user?.id) {
    userId = req.user?.id;
  } else {
    userId = req.sessionID;
  }
  if (isUserExist(userId)) {
    req.userData = (getUserAccessData(userId) as AccessData);
  } else {
    createUserInStorage(userId, {
      id: userId,
      isAuth: false,
      name: null,
      filesCounter: 0,
      extraConvertsCounter: 0,
      accessToken: null,
      refreshToken: null,
      isPaid: false,
    });
    req.userData = (getUserAccessData(userId) as AccessData);
  }
  next()
});
app.use(routes);

app.listen(process.env.APP_PORT, () => console.log(`app listening on port: ${process.env.APP_PORT}`));
console.log('env: ', process.env.TEST);
