import session from 'express-session';
import { AccessData } from '@src/storage/types';

declare module 'express-session' {
   interface SessionData {
      user: AccessData;
     }
}
declare global {
  namespace Express {
    interface Request {
      userData: AccessData;
    }
    interface User {
      id: string
    }
  }
  namespace NodeJS {
    interface ProcessEnv {
      MAX_NOT_AUTH_CONVERTS: string;
      MAX_AUTH_CONVERTS: string;
      NOT_AUTH_TIME_LIMIT: string;
      AUTH_TIME_LIMIT: string;
    }
  }
}
