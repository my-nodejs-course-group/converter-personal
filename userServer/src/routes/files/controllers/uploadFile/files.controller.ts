import { Request, Response } from 'express';
import { addFilesToUser, isPaidConvertation, checkUserStorage } from '@src/storage';
import { FileStatus, FilesData } from '@src/storage/types';
import crypto from 'crypto';
import path from 'path';
import axios from 'axios';


export const uploadFileController = async (req: Request, res: Response) => {
  const userData: FilesData = {};
  (req.files as Express.Multer.File[])?.forEach(file => {
    const fileId = crypto.randomBytes(16).toString('hex');
    const dirPathOut = path.join(path.dirname(file.destination), 'converted');
    const filePathOut = path.format({
      dir: dirPathOut,
      base: `${path.parse(file.filename).name}.webp`,
    });
    userData[fileId] = {
      filePathIn: file.path,
      filePathOut,
      originalName: file.originalname,
      status: FileStatus.pending,
    };
  });

  addFilesToUser(userData, req.userData.id);
  try {
    await axios.post(`${process.env.PROCESSING_HOST}:${process.env.PROCESSING_PORT}/files/convert`, {
      isPaid: isPaidConvertation(req.userData.id),
      userId: req.userData.id,
      filesData: checkUserStorage()[req.userData.id].files
    });
    res.json({
      status: 'ok',
    });
  } catch (error) {
    res.json({
      status: 'error',
      error
    });
  }
};
