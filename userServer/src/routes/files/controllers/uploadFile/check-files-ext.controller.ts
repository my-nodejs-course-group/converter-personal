import { NextFunction, Request, Response } from 'express';
import path from 'path';

export const checkFilesExtController = (req: Request, res: Response, next: NextFunction) => {
    if (!req.userData.isAuth) {
     for (const file of (req.files as Express.Multer.File[])) {
      const { ext } = path.parse(file.filename);
      if (ext !== '.jpg') {
        res.json({
          error: 'Please, auth if you want to sent something exept .jpg'
        });
        return;
      }
     }
    }
  next();
};
