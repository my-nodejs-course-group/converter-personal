// import { Request, Express } from 'express';

import multer from 'multer';
import path from 'path';

const MAX_FILE_SIZE = 50000000;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(process.cwd(), '/tempFiles/uploaded'));
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    cb(null, `${uniqueSuffix}-${file.originalname}`);
  },
});

export const multerSettings = multer({
  storage: storage,
  limits: { fileSize: MAX_FILE_SIZE },
});
