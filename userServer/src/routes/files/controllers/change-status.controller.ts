import { Request, Response } from 'express';
import { changeFilesStatus, isLongPollingExist, resLongPolling } from '@src/storage';
export const changeStatusController = (req: Request, res: Response) => {
  const { userId, fileId, fileStatus } = req.body;
  changeFilesStatus(userId, fileId, fileStatus);
  if (isLongPollingExist(userId)) {
    resLongPolling(userId);
  }
  res.json({
    status: 'ok'
  })
};
