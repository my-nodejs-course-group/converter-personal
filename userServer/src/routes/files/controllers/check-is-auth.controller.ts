import { NextFunction, Request, Response } from 'express';

export const checkIsAuthController = (req: Request, res: Response, next: NextFunction) => {
  if (!req.userData.isAuth) {
    res.json({
      error: 'auth first'
    })
  } else {
    next();
  }
};
