import { Request, Response } from 'express';
import { checkUserStorage } from '@src/storage';

export const checkFilesController = (req: Request, res: Response) => {
  res.json({
    status: 'ok',
    files: checkUserStorage(),
  });
};
