import { Request, Response } from 'express';
import { setLongPollingRes } from '@src/storage';

export const checkStatusController = (req: Request, res: Response) => {
  setLongPollingRes(res, req.body.userId);
};
