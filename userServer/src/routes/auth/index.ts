import { Router } from 'express';
import { adjustedPassport } from '../../authStrategy/pasport';
import { googleCallBackController } from './google-callback.controller';

const authRoutes = Router();
authRoutes.get('/google', adjustedPassport.authenticate('google'));
authRoutes.get('/google/callback', adjustedPassport.authenticate('google', { failureRedirect: '/login' }), googleCallBackController);

export { authRoutes };
