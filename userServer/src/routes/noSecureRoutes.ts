import { Router } from 'express';

import { changeStatusController } from './files//controllers/change-status.controller';

const routesNS = Router();

routesNS.use('/files/change-status', changeStatusController);

export { routesNS };
