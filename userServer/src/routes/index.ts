import { Router } from 'express';
import { authRoutes } from './auth';

import { filesRoutes } from './files';
import { healthController } from './health.controller';

const routes = Router();

routes.use('/health', healthController);
routes.use('/files', filesRoutes);
routes.use('/auth', authRoutes);

export { routes };
