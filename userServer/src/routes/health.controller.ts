import { Request, Response } from 'express';
import axios from 'axios';

export const healthController = async (req: Request, res: Response) => {
  try {
    const processingRes = await axios.get(`${process.env.PROCESSING_SERVER_HOST}:${process.env.PROCESSING_PORT}/health`);

    res.json({
      status: 'okay',
      user: req.user,
      processingStatus: processingRes.data.status,
    });
  } catch (error) {
    res.json({
      status: 'error',
      error: `processing server error: ${error}`,
    });
  }
};
