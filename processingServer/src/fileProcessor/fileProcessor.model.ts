import { FileData } from '@src/storage/types';

export interface FileQueueItem {
  fileId: string;
  userId: string;
  fileData: FileData;
}
