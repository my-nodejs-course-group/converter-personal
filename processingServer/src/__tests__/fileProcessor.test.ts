import { FileProcessor } from '../fileProcessor/fileProcessor';
import { Queue } from '../queue/queue';

describe('test file processing', () => {
  test('Queue is emplty after file processor has finished worl', async () => {
    const userQueue: Queue<string> = new Queue();
    for (let i = 0; i < 6; i = i + 1) {
      userQueue.push(`${i}`);
    }
    expect(userQueue.getSize()).toBe(6);
    const converterService = new FileProcessor(userQueue);
    converterService.startProcessing();
    await new Promise(r => setTimeout(r, 2000));
    expect(userQueue.getSize()).toBe(0);
  });
});
