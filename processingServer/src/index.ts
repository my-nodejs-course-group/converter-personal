import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';
import { routes } from './routes';

const localEnv = dotenv.config();
dotenvExpand.expand(localEnv);
const app = express();



app.use(express.urlencoded());
app.use(express.json());
app.use(morgan('dev'));
app.use(routes);

app.listen(process.env.APP_PORT, () => console.log(`app listening on port: ${process.env.APP_PORT}`));
