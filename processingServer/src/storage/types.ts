export enum FileStatus {
  wait = 'wait',
  pending = 'pending',
  ready = 'ready',
  error = 'error'
}
export interface FileData {
  filePathIn: string;
  filePathOut: string;
  originalName: string;
  status: FileStatus;
}
export interface FilesData {
  [fileId: string]: FileData;
}
export interface StorageData {
  [userId: string]: FilesData;
}
