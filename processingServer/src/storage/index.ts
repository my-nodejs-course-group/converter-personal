import { StorageData, FileStatus, FilesData } from './types';
import axios from 'axios';

const storage: StorageData = {};

export const getSotrageData = () => ({ ...storage });

export const setStorageData = (userId: string, filesData: FilesData) => {
  storage[userId] = filesData;
}
export const changeFilesStatus = (userId: string, fileId: string, status: FileStatus) => {
  storage[userId][fileId].status = status;
  if (status !== 'pending') {
    try {
      axios.post(`${process.env.USER_HOST}:${process.env.USER_SERVER_PORT}/files/change-status`, {
        userId,
        fileId,
        fileStatus: status
      })
    } catch (error) {
      console.log('failed check status: ', error);
    }
  }
};
