import { Request, Response } from 'express';
import { Queue } from '@src/queue/queue';
// import { FilesData } from '@src/storage/types';
import { setStorageData } from '@src/storage';

import { userQueue } from '@src/fileProcessor/userQueue';

import { fileProcessor, FileProcessor } from '@src/fileProcessor/fileProcessor';

export const convertFilesController = (req: Request, res: Response) => {
  const { isPaid, userId, filesData } = req.body;
  setStorageData(userId, filesData);
  if (isPaid) {
    const paidQueue = new Queue<string>();
    paidQueue.push(userId);
    const paidConverter = new FileProcessor(paidQueue);
    paidConverter.startProcessing();
  } else {
    userQueue.push(userId);
    if(userQueue.getSize() === 1) fileProcessor.startProcessing();
  }
  res.json({
    status: 'ok',
  });
};
