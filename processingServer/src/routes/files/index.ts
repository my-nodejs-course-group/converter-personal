import { Router } from 'express';
import { convertFilesController } from './controllers/convert-files.controller';

const filesRoutes = Router();

filesRoutes.post('/convert', convertFilesController);

export { filesRoutes };
