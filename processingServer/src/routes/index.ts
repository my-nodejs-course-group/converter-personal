import { Router } from 'express';

import { filesRoutes } from './files';
import { healthController } from './health.controller';

const routes = Router();

routes.use('/health', healthController);
routes.use('/files', filesRoutes);

export { routes };
